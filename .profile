# Source shell config file
if [ -f ~/.zshrc ]; then
	. ~/.zshrc
elif [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# Source aliases
if [ -f ~/.aliases ]; then
	. ~/.aliases
fi

# Set Environment Variables
if [ ! -f ~/.zshenv ]; then
	export npm_config_prefix=~/.node_modules
	export XDG_CONFIG_HOME="$HOME/.config"
fi
